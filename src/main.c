#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"


#define HEAP_SIZE 32000
#define BLOCK_SIZE 30

/* Обычное успешное выделение памяти */
static bool test1(void) {
  void *heap = heap_init(HEAP_SIZE);

  if (!heap) {
    return false;
  }

  void *block = _malloc(BLOCK_SIZE);
  struct block_header *header = block_get_header(block);
  if (!block || header->contents != block || header->is_free) {
    munmap(heap, SIZE_MAX);
    return false;
  }

  _free(block);
  munmap(heap, SIZE_MAX);
  return true;
}

/* Освобождение одного блока из нескольких выделенных */
static bool test2(void) {
  void *heap = heap_init(HEAP_SIZE);

  if (!heap) {
    return false;
  }

  void *block1 = _malloc(BLOCK_SIZE);
  if (!block1) {
    munmap(heap, SIZE_MAX);
    return false;
  }

  void *block2 = _malloc(BLOCK_SIZE);
  if (!block2) {
    _free(block1);
    munmap(heap, SIZE_MAX);
    return false;
  }

  struct block_header *header1 = block_get_header(block1);
  struct block_header *header2 = block_get_header(block2);

  _free(block1);
  if (!header1->is_free || header2->is_free) {
    _free(block2);
    munmap(heap, SIZE_MAX);
    return false;
  }

  _free(block2);
  munmap(heap, SIZE_MAX);
  return true;
}

// Освобождение двух блоков из нескольких выделенных
static bool test3(void) {
  void *heap = heap_init(HEAP_SIZE);
  if (!heap) {
    return false;
  }

  void *block1 = _malloc(BLOCK_SIZE);
  if (!block1) {
    munmap(heap, SIZE_MAX);
    return false;
  }

  void *block2 = _malloc(BLOCK_SIZE);
  if (!block2) {
    _free(block1);
    munmap(heap, SIZE_MAX);
    return false;
  }

  void *block3 = _malloc(BLOCK_SIZE);
  if (!block3) {
    _free(block2);
    _free(block1);
    munmap(heap, SIZE_MAX);
    return false;
  }

  struct block_header *h1 = block_get_header(block1);
  struct block_header *h3 = block_get_header(block3);
  _free(block2);
  _free(block1);
  if (!h1->is_free || h1->next != h3 || h1->next->contents != block3) {
    _free(block3);
    munmap(heap, SIZE_MAX);
    return false;
  }
  _free(block3);
  munmap(heap, SIZE_MAX);
  return true;
}

// Память закончилась, новый регион памяти расширяет старый
static bool test4(void) {
  void *heap = heap_init(HEAP_SIZE);
  if (!heap) {
    return false;
  }

  void *block1 = _malloc(HEAP_SIZE);
  if (!block1) {
    munmap(heap, SIZE_MAX);
    return false;
  }

  struct block_header *header1 = block_get_header(block1);

  if (header1->capacity.bytes != HEAP_SIZE) {
    _free(block1);
    munmap(heap, SIZE_MAX);
    return false;
  }

  void *block2 = _malloc(HEAP_SIZE);
  if (!block2) {
    _free(block1);
    munmap(heap, SIZE_MAX);
    return false;
  }

  struct block_header *header2 = block_get_header(block2);
  if (header2 != (struct block_header *)(header1->contents + HEAP_SIZE) ||
      header1->next != header2) {
    _free(block2);
    _free(block1);
    munmap(heap, SIZE_MAX);
    return false;
  }

  _free(block2);
  _free(block1);
  munmap(heap, SIZE_MAX);
  return true;
}

// Память закончилась, старый регион памяти не расширить
static bool test5(void) {
  void *heap = heap_init(HEAP_SIZE);
  if (!heap) {
    return false;
  }

  void *block1 = _malloc(1);
  if (!block1) {
    munmap(heap, SIZE_MAX);
    return false;
  }
  struct block_header *header1 = block_get_header(block1);

  void *padding =
      mmap(block_after(header1->next),
           2 * HEAP_SIZE, PROT_READ | PROT_WRITE,
           MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

  if (padding == MAP_FAILED ||
      padding != header1->next->contents + header1->next->capacity.bytes) {
    _free(block1);
    munmap(heap, SIZE_MAX);
    return false;
  }

  void *block2 = _malloc(10 * HEAP_SIZE);
  if (!block2) {
    _free(block1);
    munmap(heap, SIZE_MAX);
    return false;
  }

  struct block_header *header2 = block_get_header(block2);

  if (header2 ==
          (void *)(header1->next->contents + header1->next->capacity.bytes) ||
      header1->next->next ==
          (void *)(header1->next->contents + header1->next->capacity.bytes) ||
      header2 != header1->next->next) {
    _free(block1);
    _free(block2);
    munmap(heap, SIZE_MAX);
    return false;
  }

  // Free allocated space
  _free(block1);
  _free(block2);
  munmap(padding, SIZE_MAX);
  munmap(heap, SIZE_MAX);
  return true;
}

int main(void) {
  bool tests = true;
  if (test1())
    puts("test1: success");
  else {
    puts("test1: failed");
    tests = false;
  }

  if (test2())
    puts("test2: success");
  else {
    puts("test2: failed");
    tests = false;
  }

  if (test3())
    puts("test3: success");
  else {
    puts("test3: failed");
    tests = false;
  }

  if (test4())
    puts("test4: success");
  else {
    puts("test4: failed");
    tests = false;
  }

  if (test5())
    puts("test5: success");
  else {
    puts("test5: failed");
    tests = false;
  }

  if (!tests) {
    puts("tests: failed");
    return 1;
  }
  
  puts("tests: success");
  return 0;
}
