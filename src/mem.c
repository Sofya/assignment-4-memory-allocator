#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) {
  return block->capacity.bytes >= query;
}

static size_t pages_count(size_t mem) {
  return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages(size_t mem) {
  return getpagesize() * pages_count(mem);
}

static struct block_header *block_init(void *restrict addr, block_size block_sz,
                                       void *restrict next) {
  *((struct block_header *)addr) = (struct block_header){
      .next = next, .capacity = capacity_from_size(block_sz), .is_free = true};
  return (struct block_header *)addr;
}

static size_t region_actual_size(size_t query) {
  return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags) {
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE,
              MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
  size_t region_size =
      region_actual_size(size_from_capacity((block_capacity){query}).bytes);

  void *region_addr = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

  if (region_addr == MAP_FAILED || !region_addr) {
    region_addr = map_pages(addr, region_size, 0);
    if (region_addr == MAP_FAILED || !region_addr) {
      return REGION_INVALID;
    }
  }
  struct region mapped_region =
      (struct region){.addr = region_addr,
                      .size = region_size,
                      .extends = (region_addr == addr)};

  block_init(region_addr, (block_size){mapped_region.size}, NULL);
  return mapped_region;
}

void *heap_init(size_t initial) {
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;

  return region.addr;
}


/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block,
                             size_t query) {
  return block->is_free &&
         query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <=
             block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
  if (!block_splittable(block, query)) {
    return false;
  }
  // Обновление первого блока
  struct block_header *inited_block =
      block_init(block->contents + query,
                 (block_size){block->capacity.bytes - query}, block->next);
  block->capacity.bytes = query;
  block->next = inited_block;
  return true;
}

/*  --- Слияние соседних свободных блоков --- */

void *block_after(struct block_header const *block) {
  return (void *)(block->contents + block->capacity.bytes);
}
static bool blocks_continuous(struct block_header const *fst,
                              struct block_header const *snd) {
  if (fst && snd)
    return (void *)snd == block_after(fst);
  else
    return false;
}

static bool mergeable(struct block_header const *restrict fst,
                      struct block_header const *restrict snd) {
  if (fst && snd)
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
  else
    return false;
}

static bool try_merge_with_next(struct block_header *block) {
  if (block) {
    if (block->next && mergeable(block, block->next)) {
      block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
      block->next = block->next->next;
      return true;
    }
  }
  return false;
}

/*  --- ... ecли размера кучи хватает --- */
struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header *block;
};

static struct block_search_result
find_good_or_last(struct block_header *restrict block, size_t sz) {
  if (!block) {
    return (struct block_search_result){.type = BSR_CORRUPTED, .block = NULL};
  }

  struct block_header *this = block;
  struct block_header *before = NULL;

  while (this) {
    while (try_merge_with_next(this)) {
    }
    if (this->is_free && block_is_big_enough(sz, this)) {
      return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK,
                                          .block = this};
    }
    before = this;
    this = this->next;
  }
  return (struct block_search_result){.type = BSR_REACHED_END_NOT_FOUND,
                                      .block = before};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь
 расширить кучу Можно переиспользовать как только кучу расширили. */
static struct block_search_result
try_memalloc_existing(size_t query, struct block_header *block) {
  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result = find_good_or_last(block, query);

  if (result.type != BSR_FOUND_GOOD_BLOCK) {
    return result;
  }
  split_if_too_big(result.block, query);
  result.block->is_free = false;
  return result;
}

static struct block_header *grow_heap(struct block_header *restrict last,
                                      size_t query) {
  if (last) {
    struct region region = alloc_region(block_after(last), query);
    if (!region_is_invalid(&region)) {
      last->next = region.addr;
      if (last->is_free) {
        if (region.extends) {
          while (try_merge_with_next(last)) {
          }
          return last;
        }
      }
      return region.addr;
    }
  }
  return NULL;
}


/*  Реализует основную логику malloc и возвращает заголовок выделенного блока
 */
static struct block_header *memalloc(size_t query,
                                     struct block_header *heap_start) {
  if (heap_start) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result result =
        try_memalloc_existing(query, heap_start);
    if (result.type == BSR_REACHED_END_NOT_FOUND) {
      if (!grow_heap(result.block, query))
        return NULL;
      result = try_memalloc_existing(query, heap_start);
    }
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
      return result.block;
    }
  }
  return NULL;
}



void *_malloc(size_t query) {
  struct block_header *const addr =
      memalloc(query, (struct block_header *)HEAP_START);
  if (addr)
    return addr->contents;
  else
    return NULL;
}

struct block_header *block_get_header(void *contents) {
  return (struct block_header *)(((unsigned char *)contents) -
                                 offsetof(struct block_header, contents));
}

void _free(void *mem) {
  if (mem) {
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header)) {
    }
  }
}
